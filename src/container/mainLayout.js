import React, { Component } from 'react';
import { Link, Switch, Route, Redirect } from 'react-router-dom'
import { withRouter } from "react-router-dom";
import Firebase from 'firebase';
import Periodistas from '../periodistas/periodistasList'
import Consolidado from '../consolidado/main'
import Test from '../test/test'

class mainLayout extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentWillMount() {
        var user = Firebase.auth().currentUser;
        if (!user) {
            Firebase
                .auth()
                .signInWithEmailAndPassword("floyola@operatio.cl", "123456")
                .then(user => {
                    console.log("login")
                })
                .catch(error => {
                    console.log("error login")
                });
        }
    }
    render() {
        return (
            <Switch>
                <Route path="/periodistas" name="Periodistas" component={Periodistas} />
                <Route path="/consolidado" name="Consolidado" component={Consolidado} />
                <Route path="/test" name="test" component={Test} />
                <Redirect from="/" to="/consolidado" />
            </Switch>
        );
    }
}
export default mainLayout;
