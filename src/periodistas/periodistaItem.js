import React, { Component } from 'react';


class periodistaItem extends Component {

    constructor() {
        super();
        this.state = {
            actived: false
        }
    }


    
    componentWillReceiveProps(nextProps) {
        const contex = this;
        if (nextProps.periodista[110003] !== this.props.periodista[110003] || nextProps.periodista[110004] !== this.props.periodista[110004]) {
            this.setState({ actived: true })
        }
        setTimeout(function () {
            contex.setState({ actived: false });
        }, 2000);
    }

    render() {
        return (
            <article class="row fadeIn nfl" style={this.state.actived ? { background: "#279766" } : {}}>
                <ul>
                    <li>{this.props.periodista.nombre}</li>
                    <li>{this.props.periodista.comuna}</li>
                    <li>{this.props.periodista.local}</li>
                    <li>{this.props.periodista.mesa}</li>
                    <li>{this.props.periodista[110003]}</li>
                    <li>{this.props.periodista[110004]}</li>
                </ul>
            </article>
        );
    }
}

export default periodistaItem;
