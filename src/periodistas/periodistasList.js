import React, { Component } from 'react';
import Firebase from 'firebase';
import '../App.css';
import PeriodistaItem from './periodistaItem';


class periodistasList extends Component {

  constructor() {
    super();
    this.state = {
      periodistas: []
    }
  }

  componentWillMount() {
    var periodistaRef = Firebase.database().ref("periodistas").on("value", snap => {
      if (snap.val()) {
        var items = []
        snap.forEach(function (childSnapshot) {
          items.push(childSnapshot.val())
        });
        this.setState({ periodistas: items, data: snap.val() });
      }
    })
  }


  render() {
    return (
      <div className="App" style={{width  : "50%"}}>
        <section className="wrapper">
          <main class="row title">
            <ul>
              <li>Periodista</li>
              <li>Comuna</li>
              <li>Local</li>
              <li>Mesa</li>
              <li>Piñera</li>
              <li>Guiller</li>
            </ul>
          </main>
                    {
            this.state.periodistas.map((p, index) => (
              <section class="row-fadeIn-wrapper" key={index}>
                <PeriodistaItem periodista={p} />
              </section>
            ))
          } 
          
        </section>
      </div>
    );
  }
}

export default periodistasList;
