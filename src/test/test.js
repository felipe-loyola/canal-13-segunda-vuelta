import React, { Component } from 'react';
import css from './test.css'
import Firebase from 'firebase';

class test extends Component {

    constructor() {
        super();
        this.state = {
            input: "",
            comando: "",
            savedMessages: []
        }
    }

    componentWillMount() {

    }

    keyFunctions(keyCode) {
        console.log(keyCode)
        switch (keyCode) {
            case 8:
                if (this.state.comando) {
                    var str = this.state.comando.substring(0, this.state.comando.length - 1);
                    this.setState({ comando: str })
                }
                break;
            case 13:
                var arr = this.state.savedMessages;
                arr.push(this.state.comando);
                this.setState({ comando: "", savedMessages: arr });
                this.sendVotes();
            default:
                break;
        }
    }

    pushMsg(type, q, msg) {
        switch (type) {
            case "qllamadas":
                var arr = this.state.savedMessages;
                arr.push("generando " + q + " periodistas");
                this.setState({ savedMessages: arr })
                break;
            case "item":
                var arr = this.state.savedMessages;
                arr.push(msg);
                this.setState({ savedMessages: arr })
            default:
                break;
        }
    }

    sendVotes() {
        const contex = this;
        var periodistaRef = Firebase.database().ref("periodistas");
        var q = Math.floor(Math.random() * (30 - 1) + 10);
        setInterval(this.pushMsg("qllamadas", q), 3000);
        for (let index = 0; index < q; index++) {
            var num_id = Math.floor(Math.random() * (30 - 1) + 1);
            var obj = {
                nombre: "Periodita " + num_id,
                comuna: "Comuna " + Math.floor(Math.random() * (10 - 1) + 1),
                local: "Local " + Math.floor(Math.random() * (10 - 1) + 1),
                mesa: "Mesas " + Math.floor(Math.random() * (100 - 1) + 1),
                110003: Math.floor(Math.random() * (400 - 1) + 20),
                110004: Math.floor(Math.random() * (400 - 1) + 20)
            }
            periodistaRef.child("p" + num_id).set([obj][0])
            setInterval(
                this.pushMsg("item", q, "" + obj.nombre + " " + obj.comuna + " " + obj.local + " " + obj.mesa + " Piñera:" + obj[110003] + " Guiller:" + obj[110004]),
                1000);
        }

    }

    render() {
        return (
            <div>
                {
                    this.state.savedMessages.map((m, index) => (
                        <span id="terminal" key={index}>
                            {m}
                            <br />
                        </span>
                    ))
                }
                <br />
                <span id="terminal">Welcome!  {this.state.comando}</span>
                <input
                    onKeyDown={(e) => { this.keyFunctions(e.keyCode) }}
                    value={this.state.input}
                    onChange={(e) => this.setState({ comando: this.state.comando + e.target.value })}
                    style={{ background: "#FFF", color: "0f8", border: "#FFF", width: "5px" }} />
            </div>
        );
    }
}

export default test;
